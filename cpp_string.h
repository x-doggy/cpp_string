/**
 * @author Vladimir Stadnik <mailto:x-doggy at ya.ru>
 * @date 2015
 *
 * Class of string for replacing char* in classes
 */


#ifndef _CPP_STRING__
#define _CPP_STRING__

#include <iostream>
#include <cstring>


class String {
    char *data_;
    size_t size_;

public:
    struct exStringOutOfRange {};    // If incorrect index!

    String();                    // @constructor default
    String(char const, size_t);  // @constructor for repeating symbols, ex. 'zzzzzzz'
    String(char const *);        // @constructor from simple C const string
    String(String const &);      // @constructor copy
    String(String &&);           // @constructor move
    ~String();                   // @destructor

    String*     copy()   const;             // @return {String*} copy of string
    size_t      length() const;             // @return {int} size of string (amount of symbols in it)
    char const* c_str()  const;             // @return {char*} C string
    void        change(char *);             // @return {void} change string to another
    String      substr(size_t, size_t);     // @return {String} a substring from pos count times
    void        clear();

    String  operator+=(String const &);     // @return {String} Append string
    String  operator+=(char);

    String& operator=(const String &);      // String = String
    String& operator=(const char *);        // String = char*
    String& operator=(String &&);           // String = String (moving)

    const char   operator[](size_t) const;  // String s = "123"; char c = s[1];
          char&  operator[](size_t);        // String s = "123"; s[1] = 'a';
    operator char const *() const;          // Cast String obj to char*
    operator char *() const;
    operator bool() const;

    friend std::ostream &operator<<(std::ostream &, const String &);
    friend std::istream &operator>>(std::istream &, String &);
};

String operator+(String, String const &);

bool operator==(String const &, String const &);
bool operator!=(String const &, String const &);
bool operator< (String const &, String const &);
bool operator> (String const &, String const &);
bool operator<=(String const &, String const &);
bool operator>=(String const &, String const &);

std::istream &getline(std::istream &, String &);

#endif
