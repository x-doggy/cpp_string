#include "cpp_string.h"


String::String(): size_(0) {
    data_ = new char[size_+1];
    data_[0] = 0;
}

String::String(char const c, size_t sz=1): size_(sz) {
    data_ = new char[size_+1];
    for (size_t i=0; i<size_; i++) {
        data_[i] = c;
    }
    data_[size_] = 0;
}

String::String(char const *str) {
    size_ = (!str) ? 0 : strlen(str);
    data_ = new char[size_+1];
    strcpy(data_, str);
}

String::String(String const &str): size_(str.size_) {
    data_ = new char[size_+1];
    strcpy(data_, str.data_);
}

String::String(String &&str) {
    data_ = str.data_;
    size_ = str.size_;
    str.size_ = 0;
    str.data_ = nullptr;
}

String::~String() {
    delete[] data_;
}



String* String::copy() const {
    return new String(*this);
}

size_t String::length() const {
    return size_;
}

char const* String::c_str() const {
    return data_;
}

void String::change(char *str) {
    size_ = (!str) ? 0 : strlen(str);
    delete[] data_;
    data_ = new char[size_+1];
    strcpy(data_, str);
}

String String::substr(size_t pos, size_t count) {
    if (count > length())
        count = length();
    if (pos >= length() - 1)
        return "";

    String tmp;
    for (size_t i=pos; i<count; i++) {
        tmp += data_[i];
    }
    return tmp;
}


String& String::operator=(String const &str) {
    if (this != &str) {
        String tmp(str);
        std::swap(tmp, *this);
    }
    return *this;
}

String& String::operator=(char const *str) {
    String tmp(str);
    std::swap(tmp, *this);
    return *this;
}

String& String::operator=(String &&str) {
    if (this != &str) {
        size_ = str.size_;
        std::swap(data_, str.data_);
    }
    return *this;
}



char const String::operator[](size_t i) const {
    if (i<0 || i>size_) throw exStringOutOfRange();
    return data_[i];
}

char& String::operator[](size_t i) {
    if (i<0 || i>size_) throw exStringOutOfRange();
    return data_[i];
}



String::operator char const *() const {
     return *this ? data_ : (char const *) "";
}

String::operator char *() const {
    return *this ? data_ : (char *) "";
}

String::operator bool() const {
    return size_ != 0;
}



std::ostream& operator<<(std::ostream &out, String const &str) {
    out << str.data_;
    return out;
}
std::istream& operator>>(std::istream &in, String &str) {
    in >> str.data_;
    return in;
}



String String::operator+=(String const &s) {
    size_ += s.length() + 1;
    char *tmp = new char[size_];
    strcpy(tmp, data_);
    strcat(tmp, s);
    delete[] data_;
    data_ = tmp;
    return *this;
}

String String::operator+=(char c) {
    size_++;
    char *tmp = new char[size_];
    strcpy(tmp, data_);
    tmp[size_ - 1] = c;
    tmp[size_] = '\0';
    delete[] data_;
    data_ = tmp;
    return *this;
}

std::istream &getline(std::istream &in, String &str) {
    char ch;
    str.clear();
    while (in.get(ch) && ch != '\n')
        str += ch;
    return in;
}

void String::clear() {
    size_ = 1;
    delete[] data_;
    data_ = new char[size_];
    data_[0] = '\0';
}

String operator+(String s1, String const &s2) {
    return s1 += s2;
}

bool operator==(String const &s1, String const &s2) {
    return strcmp(s1, s2) == 0;
}
bool operator!=(String const &s1, String const &s2) {
    return !(s1 == s2);
}
bool operator< (String const &s1, String const &s2) {
    return strcmp(s1, s2) < 0;
}
bool operator> (String const &s1, String const &s2) {
    return s2 < s1;
}
bool operator<=(String const &s1, String const &s2) {
    return !(s2 < s1);
}
bool operator>=(String const &s1, String const &s2) {
    return !(s1 < s2);
}
